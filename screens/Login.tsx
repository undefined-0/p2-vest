import {
  Image,
  ImageBackground,
  SafeAreaView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useState} from 'react';
import tw from '../lib/tailwind';
import {useNavigation} from '@react-navigation/native';

const Login = () => {
  const navigation = useNavigation();
  const [user, setuser] = useState({
    name: '',
    password: '',
  });
  return (
    <SafeAreaView style={tw`bg-slate flex-1`}>
      <ImageBackground
        source={require('../assets/loginbg.png')}
        resizeMode="cover"
        style={tw`flex-1 p-10`}>
        <Image
          source={require('../assets/login.png')}
          style={tw`h-32 w-full mt-32`}
          resizeMode="cover"
        />
        <View style={tw``}>
          <Text style={tw`text-secondary my-4 text-lg`}>
            Login to your Account
          </Text>
          <View style={tw`relative`}>
            <Image
              source={require('../assets/people.png')}
              style={tw`h-5 w-5 left-2 top-3 absolute z-10`}
            />
            <TextInput
              style={tw`bg-white rounded-md h-12 px-8`}
              value={user.name}
              placeholder="Username"
              placeholderTextColor={'#787373'}
              onChangeText={text => setuser({...user, name: text})}
            />
          </View>
          <View style={tw`my-3 flex w-full flex-row relative `}>
            <Image
              source={require('../assets/key.png')}
              style={tw`h-5 w-5 left-2 top-3 absolute z-10`}
            />
            <TextInput
              style={tw`bg-white rounded-md h-12 px-8 relative w-full`}
              value={user.password}
              placeholder="Password"
              placeholderTextColor={'#787373'}
              onChangeText={text => setuser({...user, password: text})}
            />
            <TouchableOpacity style={tw`absolute right-3 top-4`}>
              <Text style={tw`text-primary`}>Forgot</Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            style={tw`bg-primary rounded-md h-12`}
            onPress={() => navigation.navigate('Withdraw')}>
            <Text style={tw`text-white text-center my-auto`}>Sign In</Text>
          </TouchableOpacity>
        </View>
        <TouchableOpacity
          onPress={() => navigation.navigate('Signup')}
          style={tw`mt-auto flex flex-row justify-center gap-x-1`}>
          <Text style={tw`text-[1.1rem] text-secondary`}>
            Don't have an account?
          </Text>
          <Text style={tw`text-[1.1rem] text-primary`}>Sign up</Text>
        </TouchableOpacity>
      </ImageBackground>
    </SafeAreaView>
  );
};

export default Login;
