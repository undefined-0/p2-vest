import {Image, ImageBackground, SafeAreaView} from 'react-native';
import React from 'react';
import tw from '../lib/tailwind';
import {useNavigation} from '@react-navigation/native';

const Home = () => {
  const navigation = useNavigation();
  React.useEffect(() => {
    setTimeout(() => {
      navigation.navigate('Login');
    }, 3500);
  }, []);

  return (
    <SafeAreaView style={tw`bg-slate flex-1`}>
      <ImageBackground
        source={require('../assets/town.png')}
        resizeMode="cover"
        style={tw`flex-1 p-10`}>
        <Image
          source={require('../assets/frame1.png')}
          style={tw`h-full w-full`}
          resizeMode="contain"
        />
      </ImageBackground>
    </SafeAreaView>
  );
};

export default Home;
