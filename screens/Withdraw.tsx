import {
  Image,
  SafeAreaView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useState, useMemo, useRef, useCallback} from 'react';
import tw from '../lib/tailwind';
import {useNavigation} from '@react-navigation/native';
import {BottomSheetModal} from '@gorhom/bottom-sheet';
import RNBackdrop from '../components/BackDrop';
//@ts-ignore
import OTPTextInput from 'react-native-otp-textinput';

const Withdraw = () => {
  const navigation = useNavigation();
  const bottomSheetModalRef = useRef<BottomSheetModal>(null);
  const snapPoints = useMemo(() => ['25%', '65%'], []);
  const [stage, setstage] = useState<'first' | 'second' | 'third'>('first');
  const handlePresentModalPress = useCallback(() => {
    bottomSheetModalRef.current?.present();
  }, []);
  const handleSheetChanges = useCallback((index: number) => {
    console.log('handleSheetChanges', index);
  }, []);
  return (
    <SafeAreaView style={tw`bg-white flex-1`}>
      <BottomSheetModal
        ref={bottomSheetModalRef}
        index={1}
        snapPoints={snapPoints}
        onChange={handleSheetChanges}
        backgroundStyle={{borderRadius: 30, backgroundColor: '#F2F2F2'}}
        backdropComponent={(backdropProps: any) => (
          <RNBackdrop
            {...backdropProps}
            close={() => bottomSheetModalRef.current?.close()}
          />
        )}>
        <View style={tw`flex-1 px-4`}>
          {stage === 'first' && (
            <View>
              <Text
                style={tw`text-secondary text-3xl font-semibold text-center`}>
                Transaction Summary
              </Text>
              <Text style={tw`text-secondary -mt-1 text-center`}>
                Please review the details of your transaction
              </Text>

              <View
                style={tw`flex flex-row justify-between items-center mt-7 border-b border-b-secondary pb-2`}>
                <Text style={tw`text-secondary text-lg font-semibold`}>
                  Transaction Type
                </Text>
                <Text style={tw`text-secondary text-lg font-semibold`}>
                  Wallet withdrawal
                </Text>
              </View>
              <View
                style={tw`flex flex-row justify-between items-center mt-2 border-b border-b-secondary pb-2`}>
                <Text style={tw`text-secondary text-lg font-semibold`}>
                  Amount
                </Text>
                <Text style={tw`text-secondary text-lg font-semibold`}>
                  50,000
                </Text>
              </View>
              <View
                style={tw`flex flex-row justify-between items-center mt-2 border-b border-b-secondary pb-2`}>
                <Text style={tw`text-secondary text-lg font-semibold`}>
                  Fee
                </Text>
                <Text style={tw`text-secondary text-lg font-semibold`}>25</Text>
              </View>
              <View
                style={tw`flex flex-row justify-between items-center mt-2 mb-5`}>
                <Text style={tw`text-secondary text-lg font-semibold`}> </Text>
                <Text style={tw`text-secondary text-lg font-semibold`}>
                  50,025
                </Text>
              </View>

              {/* Buttons */}
              <TouchableOpacity
                style={tw`bg-primary rounded-md h-12 mt-3`}
                onPress={() => setstage('second')}>
                <Text style={tw`text-white text-center my-auto`}>Continue</Text>
              </TouchableOpacity>
              <TouchableOpacity style={tw`bg-[#cccccc] rounded-md h-12 mt-2`}>
                <Text style={tw`text-white text-center my-auto`}>Cancel</Text>
              </TouchableOpacity>
            </View>
          )}

          {stage === 'second' && (
            <View>
              <Text
                style={tw`text-secondary text-3xl font-semibold text-center`}>
                Add New Bank
              </Text>

              <Text style={tw`text-secondary font-semibold mt-4`}>
                Account Number
              </Text>
              <TextInput style={tw`rounded-md h-10 bg-white border mt-1`} />
              <Text style={tw`text-secondary font-semibold`}>Bank Name</Text>
              <TextInput style={tw`rounded-md h-10 bg-white border mt-1`} />

              {/* Button */}
              <TouchableOpacity
                style={tw`bg-primary rounded-md h-12 mt-3`}
                onPress={() => setstage('third')}>
                <Text style={tw`text-white text-center my-auto`}>Add Bank</Text>
              </TouchableOpacity>
            </View>
          )}

          {stage === 'third' && (
            <View>
              <Text
                style={tw`text-secondary text-3xl font-semibold text-center`}>
                Enter 4 digit Pin
              </Text>
              <Text
                style={tw`text-secondary text-lg font-semibold text-center my-2`}>
                Enter your 4 Digit PIN to authorize this transaction
              </Text>
              <View style={tw`w-[80%] mx-auto mt-4`}>
                <OTPTextInput
                  textInputStyle={tw`border border-b rounded-[5px] bg-white`}
                  tintColor={'#ccc'}
                  containerStyle={tw``}
                  inputCount={4}
                  keyboardType="default"
                />
              </View>
            </View>
          )}
        </View>
      </BottomSheetModal>

      <View style={tw`px-4 py-4`}>
        <View style={tw`flex flex-row justify-between items-center`}>
          <View style={tw`flex flex-row gap-x-2 items-center`}>
            <TouchableOpacity style={tw`rounded-full p-1 bg-primary`}>
              <Image
                source={require('../assets/user.png')}
                style={tw`h-10 w-10 rounded-full`}
              />
            </TouchableOpacity>
            <Text style={tw`text-secondary text-lg font-semibold`}>
              Ajalla Ugo
            </Text>
          </View>
          <TouchableOpacity style={tw`rounded-lg p-1 h-10 w-10 bg-[#E5F4F3]`}>
            <Image
              source={require('../assets/bell.png')}
              style={tw`h-5 w-5 rounded-full m-auto`}
            />
          </TouchableOpacity>
        </View>
        {/* Second row */}
        <View style={tw`flex flex-row justify-between items-center mt-7`}>
          <Text style={tw`text-secondary text-lg font-semibold`}>
            Your Balance
          </Text>
          <View style={tw`flex flex-row justify-center items-start`}>
            <Text style={tw`text-secondary text-lg font-semibold`}>N</Text>
            <Text style={tw`text-secondary text-5xl font-semibold`}>
              120,000
            </Text>
          </View>
        </View>
        {/* Withdraw */}
        <View style={tw`mt-5`}>
          <Text style={tw`text-primary text-3xl font-semibold`}>Withdraw</Text>
          <Text style={tw`text-secondary text-lg font-semibold -mt-2`}>
            Amount
          </Text>
          <View
            style={tw`bg-[#E5F4F3] border border-primary rounded-md h-12 mt-2`}>
            <Text style={tw`text-secondary text-center my-auto text-2xl`}>
              20,000
            </Text>
          </View>
        </View>
        <View style={tw`flex flex-row justify-between items-center mt-7`}>
          <Text style={tw`text-secondary text-lg font-semibold`}>
            Select Bank
          </Text>
          <View style={tw`flex flex-row items-center gap-x-2`}>
            <Text
              style={tw`text-secondary text-sm border border-primary w-4 rounded-full  text-center`}>
              +
            </Text>
            <Text style={tw`text-secondary text-lg`}>Add New Bank</Text>
          </View>
        </View>

        <View
          style={tw`flex flex-row justify-between items-center mt-7 bg-slate rounded-md p-3`}>
          <View style={tw`flex flex-row items-center`}>
            <Image
              source={require('../assets/checked.png')}
              style={tw`h-4 w-4 mr-2`}
            />
            <View>
              <Text style={tw`text-secondary text-2xl font-semibold`}>
                Ajalla Ugo
              </Text>
              <Text style={tw`text-secondary text-lg`}>9102356712</Text>
            </View>
          </View>
          <View>
            <Text style={tw`text-secondary text-lg font-semibold`}>Bank</Text>
            <Text style={tw`text-secondary text-lg`}>Access Bank</Text>
          </View>
        </View>

        <View
          style={tw`flex flex-row justify-between items-center mt-7 bg-[#cccccc50] rounded-md p-3`}>
          <View style={tw`flex flex-row items-center`}>
            <View
              style={tw`h-4 w-4 border border-secondary rounded-full mr-3`}
            />
            <View>
              <Text style={tw`text-secondary text-2xl font-semibold`}>
                Ajalla Ugo
              </Text>
              <Text style={tw`text-secondary text-lg`}>9102356712</Text>
            </View>
          </View>
          <View>
            <Text style={tw`text-secondary text-lg font-semibold`}>Bank</Text>
            <Text style={tw`text-secondary text-lg`}>Access Bank</Text>
          </View>
        </View>

        {/* Buttons */}
        <TouchableOpacity
          style={tw`bg-primary rounded-md h-12 mt-3`}
          onPress={handlePresentModalPress}>
          <Text style={tw`text-white text-center my-auto`}>Withdraw</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={tw`bg-[#cccccc] rounded-md h-12 mt-2`}
          onPress={() => navigation.navigate('Withdraw')}>
          <Text style={tw`text-white text-center my-auto`}>Cancel</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

export default Withdraw;
