/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [],
  theme: {
    extend: {
      backgroundColor: {
        slate: '#E5F4F3',
      },
      colors: {
        primary: '#1293A9',
        secondary: '#6D6767',
      },
      borderColor: {
        primary: '#1293A9',
        secondary: '#6D6767',
      },
    },
  },
  plugins: [],
};
